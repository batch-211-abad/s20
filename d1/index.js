console.log("Hello World!")

// Repetition Control Structure (Loops)
	// Loops are one of the most important feature that programming must have
	// It lets us execute code repeatedly in a pre-set number or maybe forever

	/*
		Mini Activity
		1. Create a function named greeting() and display "Hi, Batch 211!"
		using console.log inside the function

	*/

	/*
	function greeting() {
		console.log("Hello, Batch 211!")
	};

	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();

	*/


	//we can just use loops;

	/*
	let countNum = 10;

	while(countNum !== 0){
		console.log("This is printed inside the loop: " + countNum);
		greeting();
		countNum--;
	}
	*/

	//While Loop

	/*
		A while loop takes in an expression/condition 
		If the condition evaluates to true, the statements inside the code block will be executed. 
	*/

	/*
		Syntax
			while(expression/condition) {
				statement/code block
				final expression ++/-- (iteration)
			}

			- expression/condition - this are the unit of code that is being evaluated in our loop
			- Loop will run while the condition / expression is true

			- statement/code block - code/instruction that will be executed several times.

			-finalExpression - indicates how to advance the loop.
	*/

	/* let count = 5;
	// While the value of count is not equal to 0.
	while(count !== 0){
		console.log("While: " + count);
		count --;
	}
	
	*/ 
	
	/*
	let num = 20;

	while(num !== 0){
		console.log("While: num " + num);
		num --;
	}
	*/

	/*
	let digit = 5;
	while(digit !== 20) {
		console.log("While: digit " + digit)
		digit ++;
	}
	*/


	/*
	let num1 = 1;
	while(num1 === 2){
		console.log("While: num1 " + num1);
		num1--;
	}
	*/

	//Do While Loop

	/*
		- A do-while loop works a lot like the while loop
		But unlike while loops, do-while loops guarantee that the code will be executed at least once.

	*/

	/*

		Syntax
		do {
			statement/code block
			finalExpression ++/--

		}while (expression/condition)

	*/

	/*

	How the Do While Loop Works:
	1. The statements in the "do" block executes once
	2. The message "Do While: " + number will be printed out in the console
	3. After executing once, the while statement will evaluate whether to run the next iteration of the loop based on the given expression/condition
	4. If the expression / condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met. 
	5. If the condition is true, the loop will finally stop.
	*/

	/*
	let number = Number(prompt("Give me a number: "));

	do{
		console.log("Do while: " + number)
		number += 1;
	}while (number < 10)

	*/

	//For Loop

	//A For Loop is more flexible than while and do-while loops

	/*
		It consits of three parts:
		1. Initialization value that will track the progression of the loop
		2. Expression/Condition that will be evaluated which will determine if the loop willrun one more time
		3. finalExpression indicated how to advance the loop

	*/

	/*

		Syntax 

		for (initialization; expression/condition; finalExpression) {
			statement
		}

	*/

	/*for (let count = 0; count <= 20; count++){
		console.log("For Loop: " + count);
	}
	*/

	/*
	for (let count = 0; count <= 20; count += 2){
		console.log("For Loop: " + count);
	}

	let myString = "Camille Doroteo";
	console.log(myString.length);

	for (let x = 0; x < myString.length; x++){
		console.log(myString[x])
	};

	*/

	/*

		Mini Activity
		create a variable named myFullName
		create a for loop

	*/

	let myFullName = "Giovanni Carlo Abad"

	for (let x1 = 0; x1 < myFullName.length; x1++){
		console.log(myFullName[x1])
	};

	// let myName = NehEmIAh;

	// for (let i=0; i < myName);


	//Continue and Break Statements
	/*
		The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

		The break statement is used to terminate the current loop once a match has been found

	*/

	for (let count = 0; count <= 20; count++){
		if(count % 2 === 0){

			console.log("Even Number");
			continue;

	}
	console.log("Continue and Break: " + count);
	if(count > 10){
		break;
	}
}