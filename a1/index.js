	//	1. Create a variable number that will store the value of the number provided by the user via prompt:

	//	2. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to zero and wil decrease by 1 every iteration.

	// 3. Create a condition that if the current value is less than or equal to 50, stop the loop.

	// 4. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.

	// 5 . Create another condition that if the current value is divisible by 5, print the number.


	let num = prompt("Give me a number: ");
	console.log("The number you have provided is: " + num)

	for(num; num >= 50; num--){
		if (num % 10 === 0 && num !== 50){
			console.log("The number is divisible by 10. Skipping the number");
		}
		else if (num % 5 === 0 && num !== 50) {
			console.log(num);
		}
		else if (num <= 50) {
			console.log("The current value is at 50. Terminating the loop.");
			break;
		}
		else {
			continue;
		}
	}


	// 6. Create a variable that will contain the string supercalifragilisticexpialidocious.

	// 7. Create another variable that will store the consonants from the string.

	//8. Create another Loop that will iterate through the individual letters of the string based on its length.

	//9. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true. 

	//10. Create an else statement that will add the letter to the second variable.

	let string = "supercalifragilisticexpialidocious";
	let stringConsonant = "";
	console.log(string)
	for (i = 0; i<string.length; i++){
		if (
			string[i] !== "a" &&
			string[i] !== "e" &&
			string[i] !== "i" &&
			string[i] !== "o" &&
			string[i] !== "u"
			){
			stringConsonant += string[i];
	}
		else {
			continue;
		}
	}

	console.log(stringConsonant);